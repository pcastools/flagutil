// Flagutil.go provides utility functions for working with flags

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package flagutil

import (
	"errors"
	"os"
	"strconv"
	"strings"
)

// GetPort looks up the environment variable env and converts it to an int.  It uses the default value def if env is not present in the environment.  It returns an error if the result is not in the range [1..65535].
func GetPort(env string, def int) (int, error) {
	var port int
	if p, ok := os.LookupEnv(env); !ok {
		port = def
	} else if x, err := strconv.ParseUint(strings.TrimSpace(p), 10, 16); err != nil {
		return 0, errors.New("The port must be an integer between 1 and 65535")
	} else {
		port = int(x)
	}
	return port, nil
}
